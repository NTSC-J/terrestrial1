#include "step13packet.hpp"

STEP13Packet::STEP13Packet(QString s) : IM920Packet(s)
{
    if (userdata.size() != 14)
        goto invalid;

    time = userdata.at(0) << 8 | userdata.at(1);
    rawaltitude = userdata.at(2) << 8 | userdata.at(3);
    latitude = bytesToFloat(userdata.at(4), userdata.at(5), userdata.at(6), userdata.at(7));
    longitude = bytesToFloat(userdata.at(8), userdata.at(9), userdata.at(10), userdata.at(11));
    parachute_opened = userdata.at(12) >> 7;
    power_source.or2 = ~((userdata.at(12) >> 6) & 1);
    power_source.or1 = ~((userdata.at(12) >> 5) & 1);
    rawvoltage = (userdata.at(12) << 8 | userdata.at(13)) & 8191;

    return;

invalid:
    valid = false;
}

/* Endian大丈夫か？？ */
float STEP13Packet::bytesToFloat(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3)
{
    uint8_t barray[] = { b3, b2, b1, b0 };
    float result;
    std::copy(reinterpret_cast<const uint8_t*>(&barray[0]),
              reinterpret_cast<const uint8_t*>(&barray[4]),
              reinterpret_cast<uint8_t*>(&result));
    return result;
}

QString const STEP13Packet::csvHeader = QString("node_num,module_id,rssi,time,altitude,raw_altitude,latitude,longitude,parachute,voltage,raw_voltage,power_source\r\n");
