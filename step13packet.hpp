#ifndef STEP13PACKET_HPP
#define STEP13PACKET_HPP

#include "im920packet.hpp"
#include <QString>

/**
 * @brief STEP13のパケット
 */

class STEP13Packet : public IM920Packet
{
public:
    uint16_t time;          // スタートからの時間(センチ秒)
    uint16_t rawaltitude;   // 高度(1000 * (2^16 - 1)^-1 m)
    float latitude;         // 緯度
    float longitude;        // 経度
    bool parachute_opened;  // 分離機構が作動したかどうか
    struct PowerSource {
        bool or2;           // 外部or内部
        bool or1;           // (外部or内部)or予備
    } power_source;         // 使用中の電源系統
    uint16_t rawvoltage;    // バッテリー電圧(10 * (2^13 - 1)^-1 V)

    STEP13Packet(QString s);
    double inline altitude() {
        return 1000.0 * rawaltitude / 65535;
    }

    double inline voltage() {
        return 10.0 * rawvoltage / 8191;
    }

    QString inline parachuteText() {
        return QString(parachute_opened ? "分離" : "固定");
    }

    QString inline powerSourceText() {
        if (power_source.or1) {
            return QString("予備");
        } else {
            if (power_source.or2) {
                return QString("内部");
            } else {
                return QString("外部");
            }
        }
    }

    QString inline dump() {
        if (valid)
            return QString(
                    "time: %1, "
                    "altitude: %2, "
                    "latitude: %3, "
                    "longitude: %4, "
                    "parachute: %5, "
                    "rawvoltage: %6, "
                    "voltage: %7"
                    )
                .arg(time)
                .arg(altitude())
                .arg(latitude)
                .arg(longitude)
                .arg(parachute_opened ? "open" : "closed")
                .arg(rawvoltage)
                .arg(voltage());
        else
            return QString("invalid");
    }

    QString static const csvHeader;
    // TODO: CSV出力にlibqxtとかを使う
    QString inline csvLine() {
        // TODO: 汚い
        if (valid)
            return QString("%1,%2,%3,%4,%5,%6,%7,%8,%9,%10,%11,%12\r\n")
                    .arg(node_num)
                    .arg(module_id)
                    .arg(rssi)
                    .arg(time)
                    .arg(altitude())
                    .arg(rawaltitude)
                    .arg(latitude)
                    .arg(longitude)
                    .arg(parachute_opened ? "open" : "close")
                    .arg(voltage())
                    .arg(rawvoltage)
                    .arg(powerSourceText());
        else
            return QString("-1,,,,,,,,,,,\r\n");

    }

private:
    float bytesToFloat(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3);
};

#endif // STEP13PACKET_HPP
