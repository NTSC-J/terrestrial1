#ifndef CONFIG_HPP
#define CONFIG_HPP

#include <QStandardPaths>

namespace config {
    // 設定ファイルの場所はこの設定に従う
    QString const ORGANIZATION_NAME("Tsukuba-STEP");
    QString const APPLICATION_NAME("STEP13");

    // [file]
    // ファイル出力を行うディレクトリ
    extern QString outputPath;

    // [serial]
    // シリアル通信のボーレート
    extern int baudRate;

    // [osmscout]
    // 地図データベースのディレクトリ
    extern QString databasePath;
    // スタイルシートのファイル
    extern QString stylesheetFilePath;
    // 画面のDPI (TODO: 環境から取得)
    extern double dpi;
    // 描画する地図の中心の緯度
    extern double latitude;
    // 描画する地図の中心の経度
    extern double longitude;
    // 地図の倍率
    extern double zoom;

    // [display]
    // これ以下の電圧は赤文字で表示
    extern double warnedVoltage;

    void loadConfig(); // 設定をファイルから読み込む
    void saveConfig(); // 設定をファイルに書き込む
} // namespace config

#endif // CONFIG_HPP
