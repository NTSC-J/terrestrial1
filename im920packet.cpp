#include "im920packet.hpp"

IM920Packet::IM920Packet(QString s)
{
    raw_string = s;

    QStringList sl, hl, dl;
    sl = s.split(":"); // header/userdata
    if (sl.size() != 2)
        goto invalid;

    /* header */
    hl = sl.at(0).split(",");
    if (hl.size() != 3)
        goto invalid;

    bool ok1, ok2, ok3;
    node_num = hl.at(0).toInt(&ok1, 16);
    module_id = hl.at(1).toInt(&ok2, 16);
    rssi = hl.at(2).toInt(&ok3, 16);
    if (!(ok1 && ok2 && ok3))
        goto invalid;

    /* userdata */
    dl = sl.at(1).split(",");
    userdata = QList<uint8_t>();
    for (QString s: dl) {
        bool ok;
        uint8_t d = s.toInt(&ok, 16);
        if (!ok)
            goto invalid;

        userdata.append(d);
    }

    /* fin */
    valid = true;
    return;

invalid:
    valid = false;
}

QString IM920Packet::dump()
{
    if (!valid)
        return QString("invalid\n");

    QString ret = QString("node: %1, module: %2, rssi: %3, data: %4 bytes: ").arg(QString::number(node_num)).arg(module_id).arg(rssi).arg(userdata.size());
    for (uint8_t d : userdata)
        ret += QString::number(d, 16);
    ret += "\n";
    return ret;
}
