#ifndef CONSOLEWINDOW_HPP
#define CONSOLEWINDOW_HPP

#include <QMainWindow>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QFile>
#include <QTextStream>
#include <QCloseEvent>
#include <QMenu>
#include <QVector>
#include <QVector2D>

#include <osmscout/Database.h>
#include <osmscout/MapService.h>
#include <osmscout/MapPainterQt.h>

namespace Ui {
class ConsoleWindow;
}

class ConsoleWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ConsoleWindow(QWidget *parent = 0);
    void closeEvent(QCloseEvent *event);
    ~ConsoleWindow();

private:
    Ui::ConsoleWindow *ui;
    QMenu *serialMenu;
    QSerialPort *serial;
    QFile *backupCSV;
    QFile *backupRaw;
    QTextStream *backupCSVStream;
    QTextStream *backupRawStream;

    QPixmap *pixmap;
    QPainter *painter;

    osmscout::DatabaseRef database;
    osmscout::MapServiceRef mapService;
    osmscout::StyleConfigRef styleConfig;
    std::shared_ptr<osmscout::MapPainterQt> mapPainter;
    // 緯度経度↔ピクセルの写像(手で触る)
    osmscout::MercatorProjection projection;

    QVector<QVector2D> tracks; // 地図を移動拡大縮小した時の再描画用

    void setupBackupStream();
    void setupMenuBar();
    void setupSerial();
    void setupMap();
    void setupSpinBox();
    void setupPresetComboBox();

private slots:
    void processSerial();
    void promptDatabase();
    void promptStylesheet();

    void drawMap();
    void drawTracks(float lat, float lon, bool clear = false);
    void redrawTracks();
    void redrawMapAndTracks();
};

#endif // CONSOLEWINDOW_HPP
