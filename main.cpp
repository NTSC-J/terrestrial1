#include "consolewindow.hpp"
#include <QApplication>
#include <QtSerialPort/QSerialPort>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ConsoleWindow console;
    console.show();

    return a.exec();
}
