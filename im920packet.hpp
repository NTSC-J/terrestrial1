#ifndef IM920PACKET_HPP
#define IM920PACKET_HPP

#include <QString>
#include <QList>

class IM920Packet
{
public:
    // 1行分の文字列からconstruct
    IM920Packet(QString s);
    QString dump();

    QString raw_string;
    uint8_t node_num;
    uint16_t module_id;
    uint8_t rssi;
    QList<uint8_t> userdata;
    bool valid;
};

#endif // IM920PACKET_HPP
