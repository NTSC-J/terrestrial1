#include "config.hpp"
#include <QSettings>
#include <QCoreApplication>

// デフォルト値(ファイルに書かれてなければこれが使われる)
QString config::outputPath = QStandardPaths::writableLocation(QStandardPaths::HomeLocation) + "/step13";
int config::baudRate = 38400;
double config::dpi = 96.0;
double config::latitude = 40.206556;
double config::longitude = 140.032944;
double config::zoom = 50000.0;
double config::warnedVoltage = 7.4;

// 必ず手動での設定が必要
QString config::databasePath;
QString config::stylesheetFilePath;

/**
 * @brief 設定をファイルから読み込む
 */
void config::loadConfig()
{
    QSettings settings(ORGANIZATION_NAME, APPLICATION_NAME);

    outputPath = settings.value("file/outputPath", outputPath).toString();

    baudRate = settings.value("serial/baudRate", baudRate).toInt();

    warnedVoltage = settings.value("display/warnedVoltage", warnedVoltage).toDouble();

    settings.beginGroup("osmscout");
    databasePath = settings.value("databasePath").toString();
    stylesheetFilePath = settings.value("stylesheetFilePath").toString();
    dpi = settings.value("dpi", dpi).toDouble();
    latitude = settings.value("latitude", latitude).toDouble();
    longitude = settings.value("longitude", longitude).toDouble();
    zoom = settings.value("zoom", zoom).toDouble();
    settings.endGroup();
}

/**
 * @brief 設定をファイルに書き込む
 */
void config::saveConfig()
{
    QSettings settings(ORGANIZATION_NAME, APPLICATION_NAME);

    settings.setValue("file/outputPath", outputPath);
    settings.setValue("serial/baudRate", baudRate);
    settings.setValue("display/warnedVoltage", warnedVoltage);

    settings.beginGroup("osmscout");
    settings.setValue("databasePath", databasePath);
    settings.setValue("stylesheetFilePath", stylesheetFilePath);
    settings.setValue("dpi", dpi);
    settings.setValue("latitude", latitude);
    settings.setValue("longitude", longitude);
    settings.setValue("zoom", zoom);
    settings.endGroup();
}
