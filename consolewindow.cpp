#include "consolewindow.hpp"
#include "ui_consolewindow.h"
#include "step13packet.hpp"
#include "config.hpp"

#include <QDebug>
#include <QDir>
#include <QStandardPaths>
#include <QDateTime>
#include <QMessageBox>
#include <QFileDialog>
#include <QPalette>
#include <QDoubleSpinBox>
#include <QVector2D>
#include <QTimer>

QT_USE_NAMESPACE

ConsoleWindow::ConsoleWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ConsoleWindow)
{
    ui->setupUi(this);
    config::loadConfig();
    setupSerial();
    setupMenuBar();
    setupMap();
    setupSpinBox();
    setupPresetComboBox();
    setupBackupStream();

    connect(serial, &QSerialPort::readyRead, this, &ConsoleWindow::processSerial);
}

void ConsoleWindow::setupBackupStream()
{
    if (!QDir(config::outputPath).exists()) {
        QDir().mkpath(config::outputPath);
    }
    QString fn_base = config::outputPath + "/step13-" + QDateTime::currentDateTime().toString("yyyyMMdd-hhmmsszzz");
    QString fn_csv = fn_base + ".csv";
    QString fn_raw = fn_base + ".raw";

    backupCSV = new QFile(fn_csv, this);
    if (!backupCSV->open(QIODevice::WriteOnly | QIODevice::Text)) {
        qDebug() << "failed to open backup CSV";
    }

    backupRaw = new QFile(fn_raw, this);
    if (!backupRaw->open(QIODevice::WriteOnly | QIODevice::Text)) {
        qDebug() << "failed to open backup Raw text";
    }

    backupCSVStream = new QTextStream(backupCSV);
    *backupCSVStream << STEP13Packet::csvHeader;

    backupRawStream = new QTextStream(backupRaw);
}

void ConsoleWindow::setupMenuBar()
{
    auto fileMenu = ui->menubar->addMenu(tr("ファイル(&F)"));
    fileMenu->addAction("データベース選択(&D)...", this, &ConsoleWindow::promptDatabase);
    fileMenu->addAction("スタイルシート選択(&T)...", this, &ConsoleWindow::promptStylesheet);
    fileMenu->addMenu(serialMenu); // シリアルポート選択
    fileMenu->addSeparator();
    fileMenu->addAction("終了(&X)", [this]() {close();});
}

void ConsoleWindow::setupSerial()
{
    // TODO: QTimerで数秒ごとにシリアルポートの追加とかをwatchする
    serial = new QSerialPort(this);
    serial->setBaudRate(config::baudRate);

    if (QSerialPortInfo::availablePorts().empty()) {
        QMessageBox::warning(this, tr("エラー"), tr("シリアルポートがありません"));
        ui->statusbar->showMessage("シリアルポートがありません");
    }

    serialMenu = new QMenu(tr("シリアルポート選択(&P)"), this);
    for (QSerialPortInfo const& p : QSerialPortInfo::availablePorts()) {
        QString portname = p.portName();
        serialMenu->addAction(portname,
                             [this, portname]() {
            qDebug() << "selected port: " << portname;
            serial->close();
            serial->setPort(QSerialPortInfo(portname));
            serial->open(QIODevice::ReadOnly);
        });
    }

    auto portname = QSerialPortInfo::availablePorts().at(0).portName();
    serial->setPort(QSerialPortInfo(portname));
    if (!serial->open(QIODevice::ReadOnly)) {
        QMessageBox::warning(this, tr("エラー"), tr("シリアルポートを開くのに失敗しました。"));
    }
}

void ConsoleWindow::setupMap()
{
    pixmap = new QPixmap(ui->mapLabel->width(), ui->mapLabel->height());
    pixmap->fill();
    ui->mapLabel->setPixmap(*pixmap);
    painter = new QPainter(pixmap);

    using namespace osmscout;

    database = DatabaseRef(new Database(DatabaseParameter()));
    mapService = MapServiceRef(new MapService(database));
    while (!database->Open(config::databasePath.toUtf8().data())) {
        QMessageBox::warning(this, tr("エラー"), tr("データベースを開くのに失敗しました。正しいディレクトリを選択してください。"));
        promptDatabase();
    }

    styleConfig = StyleConfigRef(new StyleConfig(database->GetTypeConfig()));
    while (!styleConfig->Load(config::stylesheetFilePath.toUtf8().data())) {
        QMessageBox::warning(this, tr("エラー"), tr("スタイルシートを開くのに失敗しました。正しいファイルを選択してください。"));
        promptStylesheet();
    }

    mapPainter = std::shared_ptr<MapPainterQt>(new MapPainterQt(styleConfig));
    //double dpi = application.screens().at(application.desktop()->primaryScreen())->physicalDotsPerInch();

    drawMap();
}

void ConsoleWindow::setupSpinBox()
{
    ui->centerLatSpinBox->setValue(config::latitude);
    ui->centerLonSpinBox->setValue(config::longitude);
    ui->zoomSpinBox->setValue(config::zoom);

    connect(ui->centerLatSpinBox, static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),
            [this](double v) {
        config::latitude = v;
        QTimer::singleShot(0, this, &ConsoleWindow::redrawMapAndTracks);
    });
    connect(ui->centerLonSpinBox, static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),
            [this](double v) {
        config::longitude = v;
        QTimer::singleShot(0, this, &ConsoleWindow::redrawMapAndTracks);
    });
    connect(ui->zoomSpinBox, static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),
            [this](double v) {
        config::zoom = v;
        QTimer::singleShot(0, this, &ConsoleWindow::redrawMapAndTracks);
    });
}

// FIXME
void ConsoleWindow::setupPresetComboBox()
{
    ui->presetComboBox->addItem("しのだ", QVector2D(40.207733, 140.027023));
    ui->presetComboBox->addItem("能代駅", QVector2D(40.206556, 140.032944));
    ui->presetComboBox->addItem("ランチャ1号機", QVector2D(40.24283719, 140.01057007));
    ui->presetComboBox->addItem("ランチャ2号機", QVector2D(40.24236787, 140.01049742));
    ui->presetComboBox->addItem("地上局", QVector2D(40.24283719, 140.0086575));
    ui->presetComboBox->addItem("テント", QVector2D(40.24245705, 140.01070526));

    connect(ui->presetComboBox, &QComboBox::currentTextChanged,
            [this](QString text) {
        QVector2D coord = ui->presetComboBox->currentData().value<QVector2D>();

        config::latitude = coord.x();
        config::longitude = coord.y();

        ui->centerLatSpinBox->setValue(config::latitude);
        ui->centerLonSpinBox->setValue(config::longitude);
        QTimer::singleShot(0, this, &ConsoleWindow::redrawMapAndTracks);
    });
}

/**
 * @brief 地図に軌跡を描く
 * @param lat 緯度
 * @param lon 経度
 */
void ConsoleWindow::drawTracks(float lat, float lon, bool clear)
{
    using namespace osmscout;

    static double x0, y0;
    double x, y;

    if (clear) {
        x0 = 0.0;
        y0 = 0.0;
    }

    if (lat == 0.0f && lon == 0.0f)
        return;

    bool b = projection.GeoIsIn(lon, lat); // なんでこの順番なんだ……
    if (!b)
        qDebug() << "Geo out of range";

    bool r = projection.GeoToPixel(GeoCoord(lat, lon), x, y);
    if (!r)
        qDebug() << "GeoToPixel failed";


    if (b && r && (x0 != 0.0 || y0 != 0.0)) {
        qDebug() << tr("line: from %1, %2 to %3, %4").arg(x0).arg(y0).arg(x).arg(y);
        painter->setPen(Qt::red);
        painter->drawLine(x0, y0, x, y);
        painter->setPen(Qt::green);
        painter->drawPoint(x, y);
        ui->mapLabel->setPixmap(*pixmap);
    }

    x0 = x;
    y0 = y;
}

void ConsoleWindow::redrawTracks()
{
    drawTracks(0.0f, 0.0f, true);
    for (QVector2D coord : tracks)
        drawTracks(coord.x(), coord.y());
}

void ConsoleWindow::redrawMapAndTracks()
{
    drawMap();
    redrawTracks();
}

void ConsoleWindow::drawMap()
{
    using namespace osmscout;

    projection.Set(GeoCoord(config::latitude, config::longitude), Magnification(config::zoom), config::dpi, ui->mapLabel->width(), ui->mapLabel->height());

    std::list<TileRef> tiles;

    mapService->LookupTiles(projection, tiles);
    mapService->LoadMissingTileData(AreaSearchParameter(), *styleConfig, tiles);
    MapData mapData;
    mapService->AddTileDataToMapData(tiles, mapData);

    MapParameter drawParameter;
    drawParameter.SetFontSize(3.0);
    mapPainter->DrawMap(projection, drawParameter, mapData, painter);

    ui->mapLabel->setPixmap(*pixmap);
}

/**
 * @brief シリアルのバッファが貯まる度に呼ばれるスロット
 */
void ConsoleWindow::processSerial()
{
    while (serial->canReadLine()) {
        auto line = QString::fromUtf8(serial->readLine());
        STEP13Packet packet(line);

#ifdef QT_DEBUG
        qDebug() << packet.dump();
#endif
        if (!packet.valid)
            continue;

        // UIに出力
        // QString::asprinf()は非推奨らしいけど気にしない
        ui->nodeNumLCD->display(packet.node_num);
        ui->rssiLCD->display(packet.rssi);
        ui->timeLCD->display(packet.time);
        ui->altitudeLCD->display(QString::asprintf("%0.2f", packet.altitude()));
        QPalette palette = ui->voltageLCD->palette();
        if (packet.voltage() <= config::warnedVoltage) {
            palette.setColor(palette.WindowText, Qt::red);
        } else {
            palette.setColor(palette.WindowText, Qt::black);
        }
        ui->voltageLCD->setPalette(palette);
        ui->voltageLCD->display(QString::asprintf("%0.3f", packet.voltage()));
        ui->latitudeLCD->display(QString::asprintf("%0.6f", packet.latitude));
        ui->longitudeLCD->display(QString::asprintf("%0.6f", packet.longitude));
        ui->parachuteLabel->setText(packet.parachuteText());
        ui->powerSourceLabel->setText(packet.powerSourceText());

        ui->powerDebugLabel->setText(tr("OR1: ") + (packet.power_source.or1 ? "1" : "0") + tr(", OR2: ") + (packet.power_source.or2 ? "1" : "0") + QString::asprintf("byte: %x", packet.userdata.at(12)));

        // 軌跡
        tracks.push_back(QVector2D(packet.latitude, packet.longitude));
        drawTracks(packet.latitude, packet.longitude);

        // ファイル出力
        *backupCSVStream << packet.csvLine();
        *backupRawStream << packet.raw_string << "\r\n";
    }
}

/**
 * @brief 地図データベースの場所をユーザに尋ねる
 */
void ConsoleWindow::promptDatabase()
{
    auto s = QFileDialog::getExistingDirectory(this, tr("データベース選択"), config::databasePath);
    if (s != "")
        config::databasePath = s;
}

/**
 * @brief 地図のスタイルシートの場所をユーザに尋ねる
 */
void ConsoleWindow::promptStylesheet()
{
    // TODO: filter
    auto s = QFileDialog::getOpenFileName(this, "スタイルシート選択");
    if (s != "")
        config::stylesheetFilePath = s;
}

void ConsoleWindow::closeEvent(QCloseEvent *event)
{
    backupCSV->flush();
    backupCSV->close();
    backupRaw->flush();
    backupRaw->close();

    config::saveConfig();

    event->accept();
}

ConsoleWindow::~ConsoleWindow()
{
    delete ui;
}
